# README #  
  
X1用の作成されたTAPファイルをWAVファイルに変換します。  
ビットレートが特定できない場合は48KHzのファイルを作成します。  
  
公開されているWAV2TAPがWindows10-64ビット環境で実行できなかったのと、  
ビルドしなおしても期待したデータが得られなかったので一部改造をしたものです。  
  
TAP2WAV_for_x1 (Ver.0.1)  
  ＴＡＰからＷＡＶへコンバートします  
  TAP2WAV_for_x1 [元ファイル.TAP] [先ファイル.WAV]  
  
MAXELL Cassette Adapterを使用してPCでWAVファイルを再生して読み込ませることが
できてます。  
ボリュームは低めの値（20/100程度）で設定すると良いでしょう。  
https://www.amazon.co.jp/gp/product/B000001OM4/ref=oh_aui_detailpage_o08_s00?ie=UTF8&psc=1  


// TAP2WAV_mod.cpp : Defines the entry point for the console application.
//
// modified for Sharp X1

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <io.h>

#define D_DEFAULT_BIT_RATE	(48000)			// X1テープフォーマット定義
#define D_X1_BIT_RATE		( 8000)			// X1テープフォーマット定義

#define D_FIXED_BIT_RATE	(0x45504154)	// "TAPE"

// TAPファイルのヘッダ
struct TAP_FILE_HEADER_CNK
{
	unsigned long	type;					// 00H : 識別インデックス
	unsigned char	name[16];				// 04H : テープの名前
	unsigned char	reserve[6];				// 15H : リザーブ
	unsigned char	write_protect;			// 1AH : ライトプロテクトノッチ
	unsigned char	write_format_type;		// 1BH : 記録フォーマットの種類
	unsigned long	bit_rate;				// 1CH : サンプリング周波数
	unsigned long	data_size;				// 20H : テープデータのサイズ
	unsigned long	tape_index;				// 24H : テープの位置
};

struct WAB_CNK {
	unsigned char name[4];
	unsigned long size;
} WCNK;

struct WAB_FMT {
	unsigned short id;      // フォーマットID
	unsigned short ch_cnt;  // チャネル数
	unsigned long  hz;      // サンプリング周波数
	unsigned long  spd;     // 平均データ速度
	unsigned short blk_siz; // ブロックサイズ
	unsigned short bit_siz; // １サンプル当たりのビット数
	unsigned short headext;	// ?
} WFMT;

unsigned char TMP0[0x1000];
unsigned char TMP1[0x4000];

short tap2wav(unsigned char *tap_name, unsigned char *wav_name)
{
	unsigned long	flng;			// データ長
	unsigned long	freq;			// ビットレート
	unsigned short	size;			// 出力データ長
	unsigned short	lng;
	unsigned short	offset;
	unsigned short	error = 0;
	unsigned char	bit_mask;
	int				magnification = 1;
	FILE *			des = NULL;
	FILE *			src = NULL;

	errno_t result_src = fopen_s(&src, (const char*)tap_name, "rb");
	errno_t result_des = fopen_s(&des, (const char*)wav_name, "wb");
	if (src == NULL || des == NULL)
	{
		if (src != NULL) fclose(src);
		if (des != NULL) fclose(des);
		remove((const char*)wav_name);
		return 1;
	}

	//-----

	flng = _filelength(_fileno(src));
	fread(&freq, 1, 4, src);
	if (freq == D_FIXED_BIT_RATE){
		// 入力ファイルのビットレートが”TAPE"マークの場合
		TAP_FILE_HEADER_CNK tap_file_header;

		fseek(src, 0, SEEK_SET);
		fread(&tap_file_header, 1, sizeof(TAP_FILE_HEADER_CNK), src);

		// X1のフォーマットで出力する
		freq			= D_DEFAULT_BIT_RATE;
		magnification = D_DEFAULT_BIT_RATE / tap_file_header.bit_rate;

		printf("freq = TAPE, genarate %ld Hz wav file.\n", freq);
	}

	//-----

	memcpy((void*)WCNK.name, (const void*)"RIFF", 4);
	// ファイル全体のサイズ−８
	WCNK.size = (flng - 4l) * 8l * magnification;
	if ((WCNK.size % 2) != 0){
		WCNK.size++;
	}
	WCNK.size += 46l - 8l;
	fwrite(&WCNK, 1, 8, des);

	fwrite("WAVE", 1, 4, des);

	//-----

	memcpy((void*)WCNK.name, (const void*)"fmt ",4);
	WCNK.size = sizeof(struct WAB_FMT);
	fwrite(&WCNK, 1, 8, des);

	WFMT.id = 1;
	WFMT.ch_cnt = 1;
	WFMT.hz = freq;
	WFMT.spd = freq;
	WFMT.blk_siz = 1;
	WFMT.bit_siz = 8;
	WFMT.headext = 0;
	fwrite(&WFMT, 1, sizeof(struct WAB_FMT), des);

	//-----

	memcpy((void*)WCNK.name, (const void*)"data",4);
	WCNK.size = (flng - 4l) * 8l * magnification; // dataのサイズは必ず偶数
	if ((WCNK.size % 2) != 0){
		WCNK.size++;
	}
	fwrite(&WCNK, 1, 8, des);

	//	unsigned long blk = 0x800l;
	unsigned long blk = 0x100l;
	while (!error && flng)
	{
		if (flng>blk)
		{
			lng = (unsigned short)blk;
			flng -= blk;
		}
		else
		{
			lng = (unsigned short)flng;
			flng = 0l;
		}

		fread(TMP0, 1, lng, src);
		if (ferror(src))
		{
			error = 1;
			break;
		}

		size = 0;
		offset = 0;
		bit_mask = 0x80;
		while (offset<lng)
		{
			int h = 0;
			for (h = 0; h < magnification; h++)
			{
				TMP1[size++] = ((TMP0[offset] & bit_mask) ? 0xff : 0x00);
			}

			bit_mask >>= 1;
			if (!bit_mask)
			{
				bit_mask = 0x80;
				offset++;
			}
		}
		fwrite(TMP1, 1, size, des);
		if (ferror(des))
		{
			error = 2;
			break;
		}
	}

	//-----

	fclose(src);
	fclose(des);
	if (error)
	{
		remove((const char*)wav_name);
	}

	return error;
}

void main(int argc, unsigned char *argv[])
{
	unsigned char *src = NULL;
	unsigned char *des = NULL;
	short i;
	short err = 0;

	for (i = 1; i<argc && !err; i++)
	{
		if ((argv[i][0] == '/') || (argv[i][0] == '-'))
		{
			err = 1;
		}
		else
		{
			if (src == NULL)
			{
				src = argv[i];
			}
			else if (des == NULL)
			{
				des = argv[i];
			}
			else
			{
				err = 1;
			}
		}
	}
	if (src == NULL || des == NULL || err)
	{
		printf("TAP2WAV (Ver.0.1)\n");
		printf("  ＴＡＰからＷＡＶへコンバートします\n");
		printf("  TAP2WAV [元ファイル.TAP] [先ファイル.WAV]\n");
		exit(1);
	}

	if (tap2wav(src, des))
	{
		printf("Error!\n");
	}
	else
	{
		printf("Ok.\n");
	}
}
